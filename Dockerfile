FROM gcc:latest
COPY . .
RUN apt install -y python3-pip cmake
RUN pip3 install conan
RUN conan install -if build -build=missing .
RUN conan build -bf build -sf . .