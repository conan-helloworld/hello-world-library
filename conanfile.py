from conans import ConanFile, CMake

class HelloWorldLibConan(ConanFile):
    name = "hello_world_lib"
    version = "0.1"
    description = "Simple hello world library."
    topics = ("hello world")
    url = "https://gitlab.com/conan-helloworld/hello-world-library"
    license = "MIT"

    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "*"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="src")
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["hello_world_lib"]
